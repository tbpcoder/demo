const request = require('supertest');
const app = require('../app.js');

let exitCode = 0;

describe('GET /', () => {
  it('responds with 200 OK', (done) => {
    request(app)
      .get('/')
      .expect(200, done);
  });
});

const fs = require('fs');

describe('File existence', () => {
  it('checks for the existence of app.js', () => {
    const filePath = './app.js';
    const fileExists = fs.existsSync(filePath);
    expect(fileExists).toBe(true);
  });
})

afterEach((done) => {
  if (currentTest.state === 'failed') {
    exitCode = 1;
  }
  done();
});

after(() => {
  process.exit(exitCode);
});
