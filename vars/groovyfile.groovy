def call(String param2) {
    script{
        if (param2 == 'awesome') { // checking parameters
            def instances = ['awesome', 'are', 'You']
            while (instances.size() > 0) {
                def instance = instances.pop()
                echo "${instance}"
            }
        } else {
            echo 'rebuild this using param2 as awesome to get a surprise'
        }
    }
}